import React, {Component} from "react";
import "../static/css/main.css";

export default class Skills extends Component{
    render(){
        return(
            <div className="d-flex justify-content-center align-items-center">
                <div className="col">
                    <div>
                        <h1 className="text-center" style={{ fontWeight: "bold"}}>SKILLS</h1>
                        <hr/>
                    </div>
                        <ul>
                            <li>
                                <h5><span className="badge badge-bg">Python</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">Django</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">HTML</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">CSS</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">Node.js</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">React</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">SQL/NoSQL</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">Git</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">Wordpress</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">Spring MVC</span></h5>
                            </li>
                            <li>
                                <h5><span className="badge badge-bg">Java</span></h5>
                            </li>
                        </ul>
                </div>
            </div>
        )
    }
}