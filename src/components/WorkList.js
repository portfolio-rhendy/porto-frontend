import React, {Component} from "react";
import WorkDataService from "../service/work.service";

export default class WorkList extends Component{
    constructor(props){
        super(props);
        this.retrieveWorks = this.retrieveWorks.bind(this);

        this.state = {
            works : []
        };
    }

    componentDidMount(){
        this.retrieveWorks();
    }

    retrieveWorks(){
        WorkDataService.getAll()
        .then(
            response =>
            {
                this.setState({
                    works : response.data
                });
                console.log(response.data);
            }
        )
        .catch(
            e => 
            {
                console.log(e);
            }
        );
    }

    render(){
        const works = this.state.works;
        return(
            <div className="d-flex justify-content-center align-items-center">
                <div className="col">
                    <div>
                        <h1 className="text-center" style={{ fontWeight: "bold"}}>WORK EXPERIENCE</h1>
                        <hr/>
                    </div>
                    <ul className='list-group'>
                        {works.map(work =>(
                            <li className = {"list-group-item-  " + work.id}>
                                <h3><b>{work.company}</b></h3>
                                {work.role} | {work.monthStart} {work.yearStart} - {work.monthEnd} {work.yearEnd}
                                <br/><em>{work.description}</em>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
}