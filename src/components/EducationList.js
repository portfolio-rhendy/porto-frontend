import React, {Component} from "react";
import EducationDataService from "../service/education.service";

export default class EducationList extends Component{
    constructor(props) {
        super(props);
        this.retrieveEducations = this.retrieveEducations.bind(this);
        
        this.state = {
            educations : []
        };
    }

    componentDidMount(){
        this.retrieveEducations();
    }

    retrieveEducations(){
        EducationDataService.getAll()
        .then(
            response => 
            {
                this.setState({
                    educations : response.data
                });
                console.log(response.data);
            }
        )
        .catch(e => {
            console.log(e);
        });
    }

    render(){
        const educations = this.state.educations;
        return (
            <div className="d-flex justify-content-center align-items-center">
                <div className="col">
                    <div>
                        <h1 className="text-center" style={{ fontWeight: "bold"}}>EDUCATION</h1>
                        <hr/>
                    </div>
                    <ul className='list-group'>
                        {educations.map(education => (
                            <li className = {"list-group-item-  " + education.id}>
                                <h3>{education.school}</h3>
                                {education.fieldOfStudy} | {education.yearStart} - {education.yearEnd ? education.yearEnd : "Present"}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }
}