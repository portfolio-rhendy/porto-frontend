import React, {Component} from "react";
import myPhoto from "../static/images/fotobaru2_cro.jpg";
import "../static/css/main.css";

export default class About extends Component{
    render(){
        return(
            <div id="about" className="bg-about d-flex justify-content-center align-items-center">
                <div className="container ">
                    <div className="row">
                        <div>
                            <h1 className="text-center" style={{ fontWeight: "bold"}}>ABOUT</h1>
                            <hr/>
                        </div>
                        <div className="col-md">
                            <div>
                            <img src={ myPhoto } className="img-fluid" alt="Me"/>
                            </div>
                        </div>
                        <div className="col-md d-flex align-items-center">
                            <div>
                            <p>
                                My name is Rhendy. I was born in Temanggung and raised in Jakarta. I am a final 
                                year Computer Science Student at University of Indonesia. Web Development has been my main
                                interest since my second year, and I am also gaining interest in Mobile Development as well 
                                as Data Science.
                            </p>
                            <p>
                                Being a computer science student, I always enjoy learning about new technologies I never have
                                experience with. I am also a responsible worker who tries to do the best in every task given,
                                in any situation. I usually work alone in the previous jobs, so I always
                                look forward to be working in a team, and meeting great people on my field! 
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}