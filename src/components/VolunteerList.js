import React, {Component} from "react";
import VolunteerDataService from "../service/volunteer.service";

export default class VolunteerList extends Component{
    constructor(props) {
        super(props);
        this.retrieveVolunteers = this.retrieveVolunteers.bind(this);

        this.state = {
            volunteers : []
        };
    }

    componentDidMount(){
        this.retrieveVolunteers();
    }

    retrieveVolunteers(){
        VolunteerDataService.getAll()
        .then(
            response =>
            {
                this.setState({
                    volunteers : response.data
                });
                console.log(response.data);
            }
        )
        .catch(e =>{
            console.log(e);
        });
    }

    render(){
        const volunteers = this.state.volunteers;
        return (
            <div className="d-flex justify-content-center align-items-center">
                <div className="col">
                    <div>
                        <h1 className="text-center" style={{ fontWeight: "bold"}}>VOLUNTEERS</h1>
                        <hr/>
                    </div>
                    <ul className='list-group'>
                        {volunteers.map(volunteer => (
                            <li className = {"list-group-item-  " + volunteer.id}>
                                <h3>{volunteer.event}</h3>
                                {volunteer.role} | {volunteer.year}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }
}