import React, {Component} from "react";
import "../static/css/main.css";


export default class Contacts extends Component{
    render(){
        return(
            <div id="home" className="d-flex justify-content-center align-items-center">
                <div className="container">
                    <div className="row">
                        <div>
                            <h1 className="text-center" style={{ fontWeight: "bold"}}>CONTACTS</h1>
                            <hr/>
                        </div>
                        <div>
                            Looking forward for any opportunities you might have to offer. Get in touch with me through any of these channels!
                        </div>
                        <div className="text-center">
                            <ul className="list-group list-group-horizontal-md text-left">
                                <li className="list-group-item border-0 flex-fill"><span className="fas fa-map-marker-alt fa-5x"/>Jakarta, Indonesia</li>
                                <li className="list-group-item border-0 flex-fill"><a href="mailto:rhendyrvld@gmail.com"><span className="far fa-envelope fa-5x"/></a>rhendyrvld@gmail.com</li>
                                <li className="list-group-item border-0 flex-fill"><a href="https://www.linkedin.com/in/rhendy-rivaldo-4b0846170/"><span className="fab fa-linkedin fa-5x"/></a>Rhendy Rivaldo</li>
                                <li className="list-group-item border-0 flex-fill"><span className="fab fa-line fa-5x"/>rhendyrivaldo</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}