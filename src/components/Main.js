import React, {Component} from "react";
import "../static/css/main.css";
import Typewriter from "typewriter-effect";

export default class Main extends Component {
    render(){
        return(
            <div id="home" className="bg-home d-flex justify-content-center align-items-center">
                <div className="container">
                    <div className="row">
                        <div className="col-md">
                            <div>
                                <h1>Hello! I am Rhendy Rivaldo</h1>
                                <h3 style={{color: "#a52a2a"}}>
                                    <Typewriter options = {{
                                        strings: ['Web Developer', 'Computer Science Student', 'Freelancer'],
                                        loop: true,
                                        autoStart: true,
                                        cursor: ' ',
                                        pauseFor: 2000
                                    }}/>
                                </h3>
                            </div>
                            <br/>
                            <div>
                                <p>Welcome to my website! You are going to know more about me here. 
                                    Please take a look!</p>
                            </div>
                        </div>
                        <div className="col-md d-md-none d-lg-block"></div>
                    </div>
                </div>
            </div>
        )
    }
}