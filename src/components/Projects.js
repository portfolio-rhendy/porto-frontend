import React, {Component} from "react";
import "../static/css/main.css";
import obi from "../static/images/obi.JPG";
import asiadrill from "../static/images/asiadrill.JPG";
import rei from "../static/images/rei.JPG";
import advocato from "../static/images/advocato.JPG";
import grublab from "../static/images/grublab.JPG";


export default class Projects extends Component{
    render(){
        return(
            <div className="d-flex justify-content-center align-items-center">
                <div className="container ">
                    <div className="row">
                        <div>
                            <h1 className="text-center" style={{ fontWeight: "bold"}}>PORTFOLIO</h1>
                            <hr/>
                        </div>
                        <div className="col-md-6">
                            <div className="card zoom-on-hover">
                                <a href="https://www.operbisnis.id/"><img src= { obi } className="card-img-top" alt="OperBisnis"/></a>
                                <div className="card-body">
                                    <h5 className="card-title">OperBisnis</h5>
                                    <em className="card-text">Website</em>
                                </div>
                            </div>
                            <br/>
                            <div className="card zoom-on-hover">
                                <a href="https://remediasienviro.com/"><img src= { rei } className="card-img-top" alt="rei"/></a>
                                <div className="card-body">
                                    <h5 className="card-title">Remediasi Enviro Indonesia</h5>
                                    <em className="card-text">Website</em>
                                </div>
                            </div>
                            <br/>
                            <div className="card zoom-on-hover">
                                <img src= { grublab } className="card-img-top" alt="grublab"/>
                                <div className="card-body">
                                    <h5 className="card-title">Grublab</h5>
                                    <em className="card-text">LINE Bot</em>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="card zoom-on-hover">
                                <a href="https://asiadrill.com/"><img src= { asiadrill } className="card-img-top" alt="asiadrill"/></a>
                                <div className="card-body">
                                    <h5 className="card-title">Asiadrill</h5>
                                    <em className="card-text">Website</em>
                                </div>
                            </div>
                            <br/>
                            <div className="card zoom-on-hover">
                                <img src= { advocato } className="card-img-top" alt="advocato"/>
                                <div className="card-body">
                                    <h5 className="card-title">Advocato</h5>
                                    <em className="card-text">Website (proprietary school project)</em>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}