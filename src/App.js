import './static/css/App.css';
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './components/Main';
import About from './components/About';
import Skills from './components/Skills';
import EducationList from './components/EducationList';
import VolunteerList from './components/VolunteerList';

import logo from './static/images/Logo.png';
import WorkList from './components/WorkList';
import Projects from './components/Projects';
import Contacts from './components/Contacts';

class App extends Component {
  render(){
    return (
      <div className="font-mont">
        <nav className="navbar navbar-expand-lg fixed-top navbar-light">
          <a className="navbar-brand" href="/#">
            <img src={logo} width="40" height="40" alt=""/>
          </a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAlt">
            <span><i className="fas fa-bars" style={{ color: "white"}}></i></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAlt">
              <ul className="navbar-nav ms-auto">
                  <li className="nav-item">
                      <a className="nav-link" href="/#home">HOME</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" href="/#about">ABOUT</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" href="/#edex">EDUCATION & EXPERIENCES</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" href="/#contacts">CONTACTS</a>
                  </li>
              </ul>
          </div>
        </nav>
        <div className="body-pad">
          <Main />
          <About />
          <div className="container" id="edex">
            <div className="row">
              <div className="col-md">
                <EducationList />
              </div>
              <div className="col-md">
                <VolunteerList />
              </div>
            </div>
            <div className="row">
              <div className="col-md">
                <WorkList />
              </div>
              <div className="col-md">
                <Skills />
              </div>
            </div>
          </div>
          <Projects />
          <Contacts />
        </div>
      </div>
    );
  }
}

export default App;
