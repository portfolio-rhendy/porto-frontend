import http from "../http-common";

class WorkDataService {
    getAll(){
        return http.get('/work');
    }

    get(id){
        return http.get('/work/' + id);
    }

    create(data){
        return http.post('/work', data);
    }
}

export default new WorkDataService();