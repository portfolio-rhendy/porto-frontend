import http from "../http-common";

class EducationDataService {
    getAll(){
        return http.get('/education');
    }

    get(id){
        return http.get('/education/' + id);
    }

    create(data){
        return http.post('/education', data);
    }
}

export default new EducationDataService();