import http from "../http-common";

class VolunteerDataService {
    getAll(){
        return http.get('/volunteer');
    }

    get(id){
        return http.get('/volunteer/' + id);
    }

    create(data){
        return http.post('/volunteer', data);
    }
}

export default new VolunteerDataService();