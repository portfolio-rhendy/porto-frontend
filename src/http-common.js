import axios from "axios";

export default axios.create({
    baseURL: "https://rrivaldo-be.herokuapp.com/api",
    headers: {
        "Content-type": "application/json"
    }
});